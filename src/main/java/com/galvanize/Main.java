package com.galvanize;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int runningTotal = 0;
        for (int i = 0; i < args.length; i++) {
           int count = Integer.parseInt(args[i]);
           if (count > 0) {
               runningTotal += count;
           }
        }
        System.out.println(runningTotal);
    }
}